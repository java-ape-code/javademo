package com.codecate.javademo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.WeakHashMap;

public class demo6 {

	public static void main(String[] args) {

//		map 的实现
//		new HashMap<K, V>();
//		new LinkedHashMap<K, V>();
//		new EnumMap<Enum<K>, V>();
//		new IdentityHashMap<K, V>();
//		new TreeMap<K, V>();
//		new Hashtable<K, V>();
//		new Properties();
//		
//		collection的实现
//		ArrayList
//		Vector
//		Stack
//		LinkedList
//		ArrayDeque
//		PriorityQueue
//		TreeSet
//		HashSet
//		LinkedHashSet

		Integer[] a = { 1, 2, 3 };
		List<Integer> b = List.of(a);
		List<Integer> c = Arrays.asList(a);
		for (Iterator iterator = c.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.println(integer);
		}

		for (Integer integer : c) {
			System.out.println(integer);
		}

		List<Integer> d = new ArrayList<>();
		for (Integer integer : d) {
			System.out.println(integer);
		}

//		d = null;
//		for (Integer integer : d) {
//			System.out.println(integer);
//		}

		List<Integer> e = new LinkedList<Integer>();
		e.add(1);
		e.add(2);
		for (Integer integer : e) {
			System.out.println(integer);
		}
		e = new Vector<Integer>();

		Map<String, Object> f = new HashMap<>();
		f.put("a", 1L);
		for (String tmp : f.keySet()) {
			System.out.println("" + tmp + ":" + f.get(tmp));
		}
		Map<String, Object> g = new TreeMap<>();
		Map<String, Object> h = new WeakHashMap<String, Object>();

	}

}
