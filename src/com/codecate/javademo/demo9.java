package com.codecate.javademo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class demo9 {
	
	public static void main(String[] args) {
		
		//@FunctionalInterface标记在接口上，“函数式接口”是指仅仅只包含一个抽象方法的接口。
		
//		Arrays.sort(null, null);
		List<String> arr = new ArrayList<>();
		arr.add("a");
		arr.add("b");
		arr.add("c");
		demo9.test(arr, (s1) -> {
			System.out.println(s1);
		});
		
		
		demo9.test(arr,demo9::test2);
		
	}
	
	public static <T>  void test(List<T> array ,demo9interface<T> c) {
		System.out.println("start");
		for (T t : array) {
			c.call(t);
		}
		System.out.println("end");
		
	}
	
	public static void test2 (String tmp) {
		System.out.println(tmp);
	}
	
	

}

