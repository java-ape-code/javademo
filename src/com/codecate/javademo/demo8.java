package com.codecate.javademo;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class demo8 {
	
	public static void main(String[] args) {
		
		long start = System.currentTimeMillis();
		for (int i=0;i<10; i++) {
			Stream<String> stream = Stream.of("A","B","C","D");
			stream.forEach(System.out::print);
		}
		long end = System.currentTimeMillis();
		System.out.println();
		System.out.println("" + (end - start) + "ms");
		
		start = System.currentTimeMillis();
		for (int i=0;i<10; i++) {
		List<String> list = List.of("A","B","C","D");
		list.forEach(System.out::print);
		}
		end = System.currentTimeMillis();
		System.out.println();
		System.out.println("" + (end - start) + "ms");
		
		
		IntStream a = IntStream.of(1,2,3,5);
		a.forEach(System.out::print);
		
	}
	

}
