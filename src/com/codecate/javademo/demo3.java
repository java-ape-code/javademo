package com.codecate.javademo;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

public class demo3 {

	public static void main(String[] args) {
		
		
//		Java提供的常用工具类有：
//
//		Math：数学计算
//
//		Random：生成伪随机数
//
//		SecureRandom：生成安全的随机数
		
		
		// 絕對值
		int a = Math.abs(-100);

		Math.max(100, 99);

		Math.min(1.2, 2.3);
		// StrictMath，它提供了和Math几乎一模一样的方法。这两个类的区别在于，
//		由于浮点数计算存在误差，不同的平台（例如x86和ARM）
//		计算的结果可能不一致（指误差不同），因此，StrictMath保证所有平台计算结果都是完全相同的，
//		而Math会尽量针对平台优化计算速度，所以，绝大多数情况下，使用Math就足够了。
		StrictMath.abs(-100);
		
		Random r = new Random();
		int randomValue = r.nextInt();
		
		
		
		SecureRandom sr = null;
        try {
            sr = SecureRandom.getInstanceStrong(); // 获取高强度安全随机数生成器
        } catch (NoSuchAlgorithmException e) {
            sr = new SecureRandom(); // 获取普通的安全随机数生成器
        }
        byte[] buffer = new byte[16];
        sr.nextBytes(buffer); // 用安全随机数填充buffer
        System.out.println(Arrays.toString(buffer));
	
	
	
	}

}
