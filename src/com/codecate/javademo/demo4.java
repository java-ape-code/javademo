package com.codecate.javademo;

public class demo4 {
	
	public static void main(String[] args) {
		
//		这是怎么肥四？为什么assert语句不起作用？
//
//		这是因为JVM默认关闭断言指令，即遇到assert语句就自动忽略了，不执行。
//
//		要执行assert语句，必须给Java虚拟机传递-enableassertions（可简写为-ea）参数启用断言。所以，上述程序必须在命令行下运行才有效果：
//
//		$ java -ea Main.java
//		Exception in thread "main" java.lang.AssertionError
//			at Main.main(Main.java:5)
		
		try {
			assert 1>0;
			assert 0>1;
			System.out.println("====");
		} catch (AssertionError e) {
			e.printStackTrace();
		
		}
		
		
		
	}

}
