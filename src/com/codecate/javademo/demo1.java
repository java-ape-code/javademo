package com.codecate.javademo;

import java.util.StringJoiner;

public class demo1 {

	public static void main(String[] args) {

		// 运行速度快慢为：StringBuilder > StringBuffer > String
		// 在线程安全上，StringBuilder是线程不安全的，而StringBuffer是线程安全的
//		String：适用于少量的字符串操作的情况
//		　　StringBuilder：适用于单线程下在字符缓冲区进行大量操作的情况
//		　　StringBuffer：适用多线程下在字符缓冲区进行大量操作的情况

		long start = System.currentTimeMillis();
		String tmp = "";
		for (int i = 0; i < 10000; i++) {
			tmp = tmp + "a" + "b" + "c";
		}
		System.out.println(tmp);
		long end = System.currentTimeMillis();
		System.out.println("" + (end - start) + "ms");

		// string builder
		start = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 10000; i++) {
			sb.append("a").append("b").append("c");
		}
		System.out.println(sb.toString());
		end = System.currentTimeMillis();
		System.out.println("" + (end - start) + "ms");

		// string buffer
		start = System.currentTimeMillis();
		StringBuffer sbuffer = new StringBuffer();
		for (int i = 0; i < 10000; i++) {
			sbuffer.append("a").append("b").append("c");
		}
		System.out.println(sbuffer.toString());
		end = System.currentTimeMillis();
		System.out.println("" + (end - start) + "ms");

		// string joiner
		StringJoiner sj = new StringJoiner(":");
		sj.add("a").add("b").add("c");
		System.out.println(sj.toString());

		// string builder  41ms
		start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			StringBuilder sb1 = new StringBuilder();
			sb1.append("a").append("b").append("c");
			System.out.print(sb1.toString());
		}
		end = System.currentTimeMillis();
		System.out.println();
		System.out.println("" + (end - start) + "ms");

		// string  24ms
		start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			String a = "a"+"b"+"c";	
			System.out.print(a);
		}
		end = System.currentTimeMillis();
		System.out.println();
		System.out.println("" + (end - start) + "ms");

	}

}
