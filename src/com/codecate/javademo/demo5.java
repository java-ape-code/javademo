package com.codecate.javademo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


//@Target
//最常用的元注解是@Target。使用@Target可以定义Annotation能够被应用于源码的哪些位置：
//
//类或接口：ElementType.TYPE；
//字段：ElementType.FIELD；
//方法：ElementType.METHOD；
//构造方法：ElementType.CONSTRUCTOR；
//方法参数：ElementType.PARAMETER。


@Target(ElementType.TYPE)
public @interface demo5 {
	String url() default "";
	
}
